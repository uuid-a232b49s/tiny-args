#if !defined(HEADER_TINY_ARGS) || defined(HEADER_TINY_ARGS_IMPL)
#error illegal include
#endif
#define HEADER_TINY_ARGS_IMPL

#include <cstring>
#include <utility>

namespace tiny_args {

    namespace impl {

        enum struct status_t { success, zeroMatch, partialMatch };

        template<typename ret_t>
        struct acceptor_invoker_t {
            template<typename acceptor_t, typename arg_t>
            static bool fn_invoke(acceptor_t & acceptor, arg_t & arg) { return acceptor(arg); }
            template<typename acceptor_t>
            static bool fn_invoke(acceptor_t & acceptor) { return acceptor(); }
        };
        template<>
        struct acceptor_invoker_t<void> {
            template<typename acceptor_t, typename arg_t>
            static bool fn_invoke(acceptor_t & acceptor, arg_t & arg) { acceptor(arg); return true; }
            template<typename acceptor_t>
            static bool fn_invoke(acceptor_t & acceptor) { acceptor(); return true; }
        };

        template<typename sub_t, typename lambda_t>
        struct action_void_sub_t : sub_t {
            lambda_t m_lambda;
            action_void_sub_t(sub_t sub, lambda_t lambda) : sub_t(std::move(sub)), m_lambda(std::move(lambda)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                switch (sub_t::fn_match(begin, end, options, rti)) {
                    case status_t::success:
                        if (acceptor_invoker_t<decltype(m_lambda())>::fn_invoke(m_lambda)) {
                            return status_t::success;
                        }
                        return status_t::zeroMatch;
                    case status_t::zeroMatch:
                        return status_t::zeroMatch;
                    default:
                        return status_t::partialMatch;
                }
            }
            template<typename stream_t>
            void fn_usage(stream_t & stream) const {
                stream << "expected ";
                sub_t::fn_printFirstMatches(stream);
                stream << "\n";
            }
        };

        template<typename sub_t, typename lambda_t>
        struct action_inject_sub_t : sub_t {
            lambda_t m_lambda;
            action_inject_sub_t(sub_t sub, lambda_t lambda) : sub_t(std::move(sub)), m_lambda(std::move(lambda)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                return sub_t::fn_match(begin, end, options, rti, m_lambda);
            }
        };

        template<typename target_t>
        struct target_setter_t {
            target_t * m_target;
            target_setter_t(target_t * target) : m_target(target) {}
            template<typename arg_t>
            void operator()(arg_t arg) const { *m_target = std::move(arg); }
        };

        template<typename flag_t>
        struct flag_increment_t {
            flag_t * m_flag;
            flag_increment_t(flag_t & flag) : m_flag(&flag) {}
            void operator()() const { ++ * m_flag; }
        };

        struct null_value {};

        template<typename x>
        struct value_holder_t {
            x m_value;
            value_holder_t(x value) : m_value(std::move(value)) {}
            template<typename stream_t>
            void printValue(stream_t & stream) const { stream << m_value; }
        };
        template<>
        struct value_holder_t<null_value> {
            constexpr value_holder_t() = default;
            constexpr value_holder_t(null_value) noexcept {}
            template<typename stream_t>
            constexpr int printValue(stream_t &) const noexcept { return 0; }
        };

        template<typename doc_t>
        struct doc_holder_t : value_holder_t<doc_t> {
            using value_holder_t<doc_t>::value_holder_t;
            template<typename stream_t>
            void printDoc(stream_t & stream) const { printValue(stream); }
        };

        template<typename x, typename y>
        inline bool compare(x & a, y & b) { return a == b; }
        template<typename x, typename y>
        inline bool compare(x * a, y * b) { return std::strcmp(a, b) == 0; }

        struct nullStream_t {
            template<typename x>
            constexpr const nullStream_t & operator<<(const x &) const noexcept { return *this; }
            constexpr const nullStream_t & flush() const noexcept { return *this; }
        } static constexpr nullStream{};

        template<typename stream_t>
        struct error_source_t {
            void(*m_usage)(const void * sub, stream_t &) = nullptr;
            const void * m_sub = nullptr;
            template<typename sub_t>
            void errorSet(const sub_t * sub) noexcept {
                m_sub = static_cast<const void *>(sub);
                m_usage = [] (const void * sub, stream_t & stream) -> void {
                    static_cast<const sub_t *>(sub)->fn_usage(stream);
                };
            }
            void errorUsage(stream_t & stream) const {
                if (m_sub) {
                    m_usage(m_sub, stream);
                } else {
                    stream << "illegal arguments\n";
                }
            }
        };
        template<>
        struct error_source_t<nullStream_t> {
            template<typename sub_t>
            void errorSet(const sub_t *) const noexcept {}
            void errorUsage(nullStream_t) const noexcept {}
        };

        struct option_parse_state_t {
            bool m_parseOptions = true;
        };

        template<typename stream_t>
        struct runtime_info_t : error_source_t<stream_t>, option_parse_state_t {};

        struct null_acceptor_t {
            template<typename arg_t>
            constexpr bool operator()(const arg_t &) const noexcept { return true; }
        };

    }

    namespace impl {

        /* https://stackoverflow.com/a/17644256 */

        template<int n, typename F, typename... T>
        struct element_at : public element_at<n - 1, T...> {};
        template<typename F, typename... T>
        struct element_at<0, F, T...> { typedef F type; };
        template<template<typename...> class base_t, int n, typename P>
        struct element {};
        template<template<typename...> class base_t, int n, typename... T>
        struct element<base_t, n, base_t<T...>> { typedef typename element_at<n, T...>::type type; };
        template<template<typename...> class base_t, typename a, typename b>
        struct tuple_concat_left {};
        template<template<typename...> class base_t, typename a, typename... b>
        struct tuple_concat_left<base_t, a, base_t<b...>> { typedef base_t<a, b...> type; };
        template<template<typename...> class base_t, typename a, typename b, int n = 0, bool ok = (n < a::size)>
            struct tuple_concat : public tuple_concat<base_t, a, b, n + 1> {
                typedef typename tuple_concat_left<
                    base_t,
                    typename element<base_t, n, a>::type,
                    typename tuple_concat<base_t, a, b, n + 1>::type
                >::type type;
            };
            template<template<typename...> class base_t, typename a, typename b, int n>
            struct tuple_concat<base_t, a, b, n, false> { typedef b type; };

            template<bool, typename>
            struct type_if_t {};
            template<typename x>
            struct type_if_t<true, x> { using t = x; };

    }

    namespace impl {

        template<typename ... sub_options_t>
        struct options_t;

        template<typename identifier_t>
        struct var_t : value_holder_t<identifier_t> {
            static constexpr int s_subCount = 0;
            static constexpr bool isOption = false;
            var_t(value_holder_t<identifier_t> identifier) : value_holder_t<identifier_t>(std::move(identifier)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t, typename acceptor_t = const null_acceptor_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti, acceptor_t & acceptor = null_acceptor_t{}) const {
                switch (options.fn_matchOpts(begin, end, rti)) {
                    case status_t::success:
                    case status_t::zeroMatch:
                        break;
                    default:
                        return status_t::partialMatch;
                }
                if (begin == end) {
                    rti.errorSet(this);
                    return status_t::zeroMatch;
                }
                if (acceptor_invoker_t<decltype(acceptor(*begin))>::fn_invoke(acceptor, *begin)) {
                    ++begin;
                    return status_t::success;
                }
                return status_t::zeroMatch;
            }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const {
                stream << "<";
                value_holder_t<identifier_t>::printValue(stream);
                stream << ">";
            }
            template<typename stream_t>
            void fn_usage(stream_t & stream) const {
                stream << "expected ";
                fn_printFirstMatches(stream);
                stream << "\n";
            }
            template<typename lambda_t>
            friend auto operator>>(var_t sub, lambda_t && lambda) -> action_inject_sub_t<var_t, lambda_t> {
                return action_inject_sub_t<var_t, lambda_t>(std::move(sub), std::move(lambda));
            }
            template<typename value_t>
            friend auto operator>>(var_t sub, value_t & value) -> action_inject_sub_t<var_t, target_setter_t<value_t>> {
                return action_inject_sub_t<var_t, target_setter_t<value_t>>(std::move(sub), target_setter_t<value_t>(&value));
            }
        };

        template<typename value_t, typename doc_t>
        struct constant_t : doc_holder_t<doc_t> {
            static constexpr int s_subCount = 0;
            static constexpr bool isOption = false;
            value_t m_value;
            constant_t(value_t value) : m_value(std::move(value)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                switch (options.fn_matchOpts(begin, end, rti)) {
                    case status_t::success:
                    case status_t::zeroMatch:
                        break;
                    default:
                        return status_t::partialMatch;
                }
                if (begin == end) {
                    rti.errorSet(this);
                    return status_t::zeroMatch;
                }
                if (compare(m_value, *begin)) {
                    ++begin;
                    return status_t::success;
                }
                return status_t::zeroMatch;
            }
            template<typename stream_t>
            void fn_usage(stream_t & stream) const { stream << "expected '" << m_value << "'\n"; }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const { stream << m_value; }
            template<typename lambda_t>
            friend auto operator>>(constant_t sub, lambda_t && lambda) -> action_void_sub_t<constant_t, lambda_t> {
                return action_void_sub_t<constant_t, lambda_t>(std::move(sub), std::move(lambda));
            }
            template<typename flag_t>
            friend auto operator>>(constant_t sub, flag_t & flag) -> action_void_sub_t<constant_t, flag_increment_t<flag_t>> {
                return action_void_sub_t<constant_t, flag_increment_t<flag_t>>(std::move(sub), flag_increment_t<flag_t>(flag));
            }
        };

        template<typename ... subs_t>
        struct all_t;
        template<>
        struct all_t<> {
            static constexpr int s_subCount = 0;
            static constexpr bool isOption = false;
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            constexpr status_t fn_match(const begin_t &, const end_t &, const options_t &, rti_t &) const noexcept { return status_t::success; }
            template<status_t, typename begin_t, typename end_t, typename options_t, typename stream_t>
            constexpr status_t fn_match0(begin_t &, const end_t &, const options_t &, stream_t &) const noexcept { return status_t::success; }
            template<typename stream_t>
            constexpr int fn_printFirstMatches(stream_t &) const noexcept { return 0; }
        };
        template<typename sub_t, typename ... subs_t>
        struct all_t<sub_t, subs_t...> : all_t<subs_t...> {
            static constexpr int s_subCount = sizeof...(subs_t);
            static constexpr bool isOption = false;
            sub_t m_sub;
            all_t(sub_t sub, subs_t ... subs) : all_t<subs_t...>(std::move(subs)...), m_sub(std::move(sub)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                auto j = begin;
                switch (fn_match0<status_t::zeroMatch>(j, end, options, rti)) {
                    case status_t::success:
                        begin = j;
                        return status_t::success;
                    case status_t::zeroMatch:
                        return status_t::zeroMatch;
                    default:
                        return status_t::partialMatch;
                }
            }
            template<status_t partFail, typename begin_t, typename end_t, typename options_t, typename rti_t, typename sub_t_ = sub_t>
            auto fn_match0(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const -> typename impl::type_if_t<!sub_t_::isOption, status_t>::t {
                if (begin == end) {
                    rti.errorSet(this);
                    return partFail;
                }
                switch (m_sub.fn_match(begin, end, options, rti)) {
                    case status_t::success:
                        switch (all_t<subs_t...>::template fn_match0<status_t::partialMatch>(begin, end, options, rti)) {
                            case status_t::success:
                                switch (options.fn_matchOpts(begin, end, rti)) {
                                    case status_t::success:
                                    case status_t::zeroMatch:
                                        return status_t::success;
                                    default:
                                        return status_t::partialMatch;
                                }
                            default:
                                return status_t::partialMatch;
                        }
                    case status_t::zeroMatch:
                        return partFail;
                    default:
                        return status_t::partialMatch;
                }
            }
            template<status_t partFail, typename begin_t, typename end_t, typename options_t, typename rti_t, typename sub_t_ = sub_t>
            auto fn_match0(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const -> typename impl::type_if_t<sub_t_::isOption, status_t>::t {
                return all_t<subs_t...>::template fn_match0<partFail>(begin, end, typename tuple_concat<impl::options_t, impl::options_t<sub_t_>, options_t>::type(m_sub, options), rti);
            }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const { return m_sub.fn_printFirstMatches(stream); }
            template<typename stream_t>
            void fn_usage(stream_t & stream) const {
                stream << "expected [ ";
                m_sub.fn_printFirstMatches(stream);
                stream << " ]\n";
            }
        };

        template<typename ... subs_t>
        struct any_t;
        template<>
        struct any_t<> {
            static constexpr int s_subCount = 0;
            static constexpr bool isOption = false;
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            constexpr status_t fn_match(const begin_t &, const end_t &, const options_t &, rti_t &) const noexcept { return status_t::success; }
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            constexpr status_t fn_match0(begin_t &, end_t &, const options_t &, rti_t &) const noexcept { return status_t::zeroMatch; }
            template<typename stream_t>
            constexpr int fn_printFirstMatches(stream_t &) const noexcept { return 0; }
            template<typename stream_t>
            constexpr int fn_printFirstMatches0(stream_t &) const noexcept { return 0; }
        };
        template<typename sub_t, typename ... subs_t>
        struct any_t<sub_t, subs_t...> : any_t<subs_t...> {
            static constexpr int s_subCount = sizeof...(subs_t);
            static constexpr bool isOption = false;
            sub_t m_sub;
            any_t(sub_t sub, subs_t ... subs) : any_t<subs_t...>(std::move(subs)...), m_sub(std::move(sub)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                if (begin == end) {
                    rti.errorSet(this);
                    return status_t::zeroMatch;
                }
                return fn_match0(begin, end, options, rti);
            }
            template<typename begin_t, typename end_t, typename options_t, typename rti_t, typename sub_t_ = sub_t>
            auto fn_match0(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const -> typename impl::type_if_t<!sub_t_::isOption, status_t>::t {
                switch (m_sub.fn_match(begin, end, options, rti)) {
                    case status_t::success:
                        switch (options.fn_matchOpts(begin, end, rti)) {
                            case status_t::success:
                            case status_t::zeroMatch:
                                return status_t::success;
                            default:
                                return status_t::partialMatch;
                        }
                    case status_t::zeroMatch:
                        return any_t<subs_t...>::fn_match0(begin, end, options, rti);
                    default:
                        return status_t::partialMatch;
                }
            }
            template<typename begin_t, typename end_t, typename options_t, typename rti_t, typename sub_t_ = sub_t>
            auto fn_match0(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const -> typename impl::type_if_t<sub_t_::isOption, status_t>::t {
                return any_t<subs_t...>::fn_match0(begin, end, typename tuple_concat<impl::options_t, impl::options_t<sub_t_>, options_t>::type(m_sub, options), rti);
            }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const {
                m_sub.fn_printFirstMatches(stream);
                any_t<subs_t...>::fn_printFirstMatches0(stream);
            }
            template<typename stream_t>
            void fn_printFirstMatches0(stream_t & stream) const {
                stream << " | ";
                m_sub.fn_printFirstMatches(stream);
                any_t<subs_t...>::fn_printFirstMatches0(stream);
            }
            template<typename stream_t>
            void fn_usage(stream_t & stream) const {
                stream << "expected [ ";
                fn_printFirstMatches(stream);
                stream << " ]\n";
            }
        };

        template<typename sub_t>
        struct loop_t {
            static constexpr int s_subCount = sub_t::s_subCount;
            static constexpr bool isOption = false;
            sub_t m_sub;
            loop_t(sub_t sub) : m_sub(std::move(sub)) {}
            template<typename begin_t, typename end_t, typename options_t, typename rti_t>
            status_t fn_match(begin_t & begin, const end_t & end, const options_t & options, rti_t & rti) const {
                switch (m_sub.fn_match(begin, end, options, rti)) {
                    case status_t::success:
                    lbl_loop:
                        switch (m_sub.fn_match(begin, end, options, rti)) {
                            case status_t::success:
                                goto lbl_loop;
                            case status_t::zeroMatch:
                                return status_t::success;
                            default:
                                return status_t::partialMatch;
                        }
                    case status_t::zeroMatch:
                        return status_t::zeroMatch;
                    default:
                        return status_t::partialMatch;
                }
            }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const { return m_sub.fn_printFirstMatches(stream); }
        };

        template<typename ... sub_options_t>
        struct options_t;
        template<>
        struct options_t<> {
            static constexpr unsigned int size = 0;
            template<typename begin_t, typename end_t, typename rti_t>
            constexpr status_t fn_matchOpts(begin_t &, const end_t &, const rti_t &) const noexcept { return status_t::success; }
            template<typename begin_t, typename end_t, typename rti_t>
            constexpr status_t fn_matchOpts0(begin_t &, const end_t &, const rti_t &) const noexcept { return status_t::zeroMatch; }
        };
        template<typename sub_option_t, typename ... sub_options_t>
        struct options_t<sub_option_t, sub_options_t...> : options_t<sub_options_t...> {
            static constexpr unsigned int size = sizeof...(sub_options_t) + 1;
            const sub_option_t & m_subOption;
            options_t(const sub_option_t & sub, const options_t<sub_options_t...> & subs) : options_t<sub_options_t...>(subs), m_subOption(sub) {}
            template<typename begin_t, typename end_t, typename rti_t>
            status_t fn_matchOpts(begin_t & begin, const end_t & end, rti_t & rti) const {
            lbl_loop:
                switch (fn_matchOpts0(begin, end, rti)) {
                    case status_t::success:
                        goto lbl_loop;
                    case status_t::zeroMatch:
                        return status_t::success;
                    default:
                        return status_t::partialMatch;
                }
            }
            template<typename begin_t, typename end_t, typename rti_t>
            status_t fn_matchOpts0(begin_t & begin, const end_t & end, rti_t & rti) const {
                switch (options_t<sub_options_t...>::fn_matchOpts0(begin, end, rti)) {
                    case status_t::success:
                    case status_t::zeroMatch:
                        return m_subOption.fn_matchOpt(begin, end, rti);
                    default:
                        return status_t::partialMatch;
                }
            }
        };

        template<typename sub_t>
        struct option_t {
            static constexpr bool isOption = true;
            sub_t m_sub;
            option_t(sub_t sub) : m_sub(std::move(sub)) {}
            template<typename begin_t, typename end_t, typename rti_t>
            status_t fn_matchOpt(begin_t & begin, const end_t & end, rti_t & rti) const {
                if (!rti.m_parseOptions) { return status_t::zeroMatch; }
                if (begin != end) {
                    if (impl::compare("--", *begin)) {
                        ++begin;
                        rti.m_parseOptions = false;
                        return status_t::zeroMatch;
                    }
                }
                return m_sub.fn_match(begin, end, options_t<>(), rti);
            }
            template<typename stream_t>
            void fn_printFirstMatches(stream_t & stream) const {
                m_sub.fn_printFirstMatches(stream);
            }
        };

        template<typename sub_t, typename stream_t>
        static void usage0(const sub_t & sub, stream_t & stream) {
            stream << "expected [ ";
            sub.fn_printFirstMatches(stream);
            stream << " ]\n";
            stream.flush();
        }

        template<typename sub_t, typename begin_t, typename end_t, typename stream_t>
        static bool parse0(const sub_t & sub, begin_t begin, end_t end, stream_t & stream) {
            if (begin == end) {
                usage0(sub, stream);
                return false;
            } else {
                runtime_info_t<stream_t> rti;
                switch (sub.fn_match(begin, end, options_t<>(), rti)) {
                    case status_t::success:
                        if (begin == end) { return true; }
                        stream << "unexpected extra arguments:";
                        for (; begin != end; ++begin) { stream << " " << *begin; }
                        stream << "\n";
                        break;
                    case status_t::zeroMatch:
                        usage0(sub, stream);
                        return false;
                    case status_t::partialMatch:
                        rti.errorUsage(stream);
                        break;
                }
                stream.flush();
                return false;
            }
        }

    }

}
