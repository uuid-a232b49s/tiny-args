#include <vector>
#include <cassert>
#include <cstring>
#include <iostream>

#include "tiny-args.hpp"

int main(int, char **) {
    using namespace tiny_args;
    using namespace tiny_args::matcher;

    std::vector<const char *> paths;
    const char * path = "";
    int force = 0;
    auto clearVars = [&] {
        paths.clear();
        path = "";
        force = 0;
    };

    auto fsCmd = any(
        all(constant("dir"),
            any(
                all(constant("create"), loop(var("path") >> [&] (const char * x) { paths.emplace_back(x); })),
                all(constant("delete"), option(constant("--force") >> force), var("path") >> path)
            )
        ),
        all(constant("list"), var("path") >> [&] (const char * x) { path = x; })
    );

    {
        clearVars();
        auto argv = { "dir", "create", "./a" };
        assert(parse(fsCmd, argv.begin(), argv.end()));
        assert(*path == 0);
        assert(paths.size() == 1);
        assert(std::strcmp(paths[0], "./a") == 0);
    }

    {
        clearVars();
        auto argv = { "dir", "create", "./a", "./b", "./c" };
        assert(parse(fsCmd, argv.begin(), argv.end()));
        assert(*path == 0);
        assert(paths.size() == 3);
        assert(std::strcmp(paths[0], "./a") == 0);
        assert(std::strcmp(paths[1], "./b") == 0);
        assert(std::strcmp(paths[2], "./c") == 0);
    }

    {
        clearVars();
        auto argv = { "dir", "delete", "./b" };
        assert(parse(fsCmd, argv.begin(), argv.end()));
        assert(!force);
        assert(std::strcmp(path, "./b") == 0);
    }

    {
        clearVars();
        auto argv = { "dir", "delete", "--force", "./b" };
        assert(parse(fsCmd, argv.begin(), argv.end()));
        assert(force);
        assert(std::strcmp(path, "./b") == 0);
    }

    {
        clearVars();
        auto argv = { "list", "./c" };
        assert(parse(fsCmd, argv.begin(), argv.end()));
        assert(std::strcmp(path, "./c") == 0);
    }

}
