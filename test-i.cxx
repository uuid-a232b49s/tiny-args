#include <vector>
#include <cassert>
#include <cstring>
#include <iostream>

#include "tiny-args.hpp"

int main(int argc, char * * argv) {
    using namespace tiny_args;
    using namespace tiny_args::matcher;

    const char * path = "";
    int create_parents = 0;
    int force = 0;
    int truncate = 0;

    auto cmd = any(
        all(constant("list"), var("path") >> path),
        all(constant("create"), any(
            option(constant("--create-parents") >> create_parents),
            option(all(constant("--set-perm"), var("permission"))),
            all(constant("dir"), var("path") >> path),
            option(constant("--truncate") >> truncate),
            all(constant("file"), var("path") >> path)
        )),
        all(constant("delete"), option(any(constant("--force") >> force, constant("-f") >> force)), var("path") >> path)
    );

    auto r = parse(cmd, argv + 1, argv + argc, std::cerr);

    std::cerr << "parsed           = [" << r << "]" << std::endl;
    std::cerr << "path             = [" << path << "]" << std::endl;
    std::cerr << "--force          = [" << force << "]" << std::endl;
    std::cerr << "--create-parents = [" << create_parents << "]" << std::endl;
    std::cerr << "--truncate       = [" << truncate << "]" << std::endl;

    return r ? 0 : 1;
}
