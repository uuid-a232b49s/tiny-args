#ifndef HEADER_TINY_ARGS
#define HEADER_TINY_ARGS 1

#include <utility>

#include "tiny-args-impl.inl"

namespace tiny_args {

    namespace matcher {

        // matches any variable
        // invokes acceptor lambda with the matched argument
        // accept may optionally return a bool-convertible value to indicate whether the argument was accepted
        template<typename identifier_t = impl::null_value>
        inline static auto var(
            // argument identifier
            identifier_t identifier = {}
        ) -> impl::var_t<identifier_t> {
            return impl::var_t<identifier_t>(impl::value_holder_t<identifier_t>(std::move(identifier)));
        }

        // matches a constant
        template<typename value_t>
        inline static auto constant(
            // constant value to match
            value_t value
        ) -> impl::constant_t<value_t, impl::null_value> {
            return impl::constant_t<value_t, impl::null_value>(std::move(value));
        }

        // matches all sub-matchers
        template<typename ... subs_t>
        inline static auto all(subs_t ... subs) -> impl::all_t<subs_t...> { return impl::all_t<subs_t...>(std::move(subs)...); }
        inline static auto all() ->impl::all_t<> = delete;

        // matches any one of the sub-matchers
        // matching will stop when the first sub-matcher fully matches the arguments
        template<typename ... subs_t>
        inline static auto any(subs_t ... subs) -> impl::any_t<subs_t...> { return impl::any_t<subs_t...>(std::move(subs)...); }
        inline static auto any() -> impl::any_t<> = delete;

        // matches the argument in a loop for as long as possible
        template<typename sub_t>
        inline static auto loop(sub_t sub) -> impl::loop_t<sub_t> { return impl::loop_t<sub_t>(std::move(sub)); }

        // optionally matches the sub-matcher within the current [all] or [any] context
        // '--' argument will prevent any trailing options from being parsed
        template<typename sub_t>
        inline static auto option(sub_t sub) -> impl::option_t<sub_t> { return impl::option_t<sub_t>(std::move(sub)); };

    }

    // parses arguments
    template<typename sub_t, typename begin_t, typename end_t>
    inline static bool parse(
        // sub-matcher to use in parsing
        const sub_t & sub,
        // arguments begin iterator
        begin_t begin,
        // arguments end iterator
        end_t end
    ) { return impl::parse0(sub, begin, end, impl::nullStream); }

    // parses arguments
    template<typename sub_t, typename begin_t, typename end_t, typename printUsageStream_t>
    inline static bool parse(
        // sub-matcher to use in parsing
        const sub_t & sub,
        // arguments begin iterator
        begin_t begin,
        // arguments end iterator
        end_t end,
        // stream to print usage to
        printUsageStream_t & stream
    ) { return impl::parse0(sub, begin, end, stream); }

    // print usafe of the matcher
    template<typename sub_t, typename stream_t>
    inline static void usage(const sub_t & sub, stream_t & stream) { impl::usage0(sub, stream); }

}

#endif
