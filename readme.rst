tiny-args
=========


What is this?
-------------

- A tiny cli arg parser lib


License
-------

Boost Software License - Version 1.0


CMake
-----

- interface library target: ``tiny-args``

- tests variable: ``TINYARGS_TESTS``

  - ``ON`` by default


Tested compilers
----------------

- gnu 10.2.1

- clang 11

- msvc 19
